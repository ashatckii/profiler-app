export const ROOT_ROUTES = {
  HOME: '/',
  PROFILES: '/profiles',
}

const { PROFILES } = ROOT_ROUTES

export const PROFILES_ROUTES = {
  CREATE_PROFILE: `${PROFILES}/create-profile`,
  VIEW_PROFILE: {
    PATH: `${PROFILES}/view-profile/:id`,
    buildPath: (id: string) => `${PROFILES}/view-profile/${id}`,
  },
  EDIT_PROFILE: {
    PATH: `${PROFILES}/edit-profile/:id`,
    buildPath: (id: string) => `${PROFILES}/edit-profile/${id}`,
  },
}
