import { Layout } from 'antd'
import React, { ReactNode } from 'react'
import Navbar from '../components/Navbar'

const { Header, Content } = Layout

type CommonLayoutProps = {
  children: ReactNode
}

const CommonLayout = ({ children }: CommonLayoutProps) => {
  return (
    <Layout>
      <Header>
        <Navbar />
      </Header>
      <Content style={{ height: 'calc(100vh - 64px)', padding: '80px 120px' }}>
        {children}
      </Content>
    </Layout>
  )
}

export default CommonLayout
