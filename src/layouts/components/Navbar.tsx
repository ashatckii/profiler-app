import { Menu } from 'antd'
import React from 'react'
import { Link } from 'react-router-dom'
import { ROOT_ROUTES } from '../../constants/routes'

const { HOME, PROFILES } = ROOT_ROUTES

const navbarMenu = [
  {
    id: 'home',
    title: 'Home',
    href: HOME,
  },
  {
    id: 'profiles',
    title: 'Profiles',
    href: PROFILES,
  },
]

const Navbar = () => {
  return (
    <Menu onClick={() => {}} mode="horizontal">
      {navbarMenu.map(it => (
        <Menu.Item key={it.id}>
          <Link to={it.href}>{it.title}</Link>
        </Menu.Item>
      ))}
    </Menu>
  )
}

export default Navbar
