import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { PROFILES_ROUTES, ROOT_ROUTES } from '../constants/routes'
import Home from '../screens/Home'
import CreateProfile from '../screens/profiles/CreateProfile'
import DetailsProfile from '../screens/profiles/DetailsProfile'
import EditProfile from '../screens/profiles/EditProfile'
import Profiles from '../screens/profiles/Profiles'

const { HOME, PROFILES } = ROOT_ROUTES

const AppRoutes = () => {
  return (
    <Routes>
      <Route path={HOME} element={<Home />} />
      <Route
        path={PROFILES_ROUTES.CREATE_PROFILE}
        element={<CreateProfile />}
      />
      <Route
        path={PROFILES_ROUTES.VIEW_PROFILE.PATH}
        element={<DetailsProfile />}
      />
      <Route
        path={PROFILES_ROUTES.EDIT_PROFILE.PATH}
        element={<EditProfile />}
      />
      <Route path={PROFILES} element={<Profiles />} />
    </Routes>
  )
}

export default AppRoutes
