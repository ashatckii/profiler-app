import Title from 'antd/lib/typography/Title'
import React from 'react'
import { Link } from 'react-router-dom'
import { ROOT_ROUTES } from '../../constants/routes'
import CommonLayout from '../../layouts/CommonLayout'

const { PROFILES } = ROOT_ROUTES

const Home = () => {
  return (
    <CommonLayout>
      <Title>Profiler App</Title>

      <Link to={PROFILES} style={{ fontSize: '24px' }}>
        Explore Profiles
      </Link>
    </CommonLayout>
  )
}

export default Home
