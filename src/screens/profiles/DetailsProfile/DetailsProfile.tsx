import { Button, Typography } from 'antd'
import Title from 'antd/lib/typography/Title'
import React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { PROFILES_ROUTES } from '../../../constants/routes'
import CommonLayout from '../../../layouts/CommonLayout'
import { useAppSelector } from '../../../store'

const { EDIT_PROFILE } = PROFILES_ROUTES

const DetailsProfile = () => {
  const { id: profileId } = useParams()
  const profilesList = useAppSelector(state => state.profiles.list)
  const currentProfile = profilesList.find(it => it.id === profileId)

  const navigate = useNavigate()

  const handleEditClick = () => {
    if (!profileId) return null
    navigate(EDIT_PROFILE.buildPath(profileId))
  }

  return (
    <CommonLayout>
      <Title>{`Profile details: ${currentProfile?.firstName} ${currentProfile?.lastName}`}</Title>

      <div style={{ display: 'flex', flexDirection: 'column' }}>
        {currentProfile?.skills.map((it, i) => (
          <Typography.Text key={it.id}>
            Skill №{i + 1}: {it.title}
          </Typography.Text>
        ))}
      </div>

      <div style={{ marginTop: '30px' }}>
        <Button type="primary" onClick={handleEditClick}>
          Edit profile
        </Button>
      </div>
    </CommonLayout>
  )
}

export default DetailsProfile
