import { nanoid } from '@reduxjs/toolkit'
import { notification } from 'antd'
import Title from 'antd/lib/typography/Title'
import { Formik, FormikHelpers } from 'formik'
import { AddRowButton, Form, Input, SubmitButton, Table } from 'formik-antd'
import React from 'react'
import { useParams } from 'react-router-dom'
import * as yup from 'yup'
import CommonLayout from '../../../layouts/CommonLayout'
import { useAppDispatch, useAppSelector } from '../../../store'
import {
  Profile,
  updateProfile,
} from '../../../store/slices/profiles/profilesSlice'

const validationSchema = yup.object({
  firstName: yup.string().required(),
  lastName: yup.string().required(),
  email: yup.string().email().required(),
  tagline: yup.string(),
  experiences: yup.array().of(
    yup.object().shape({
      company: yup.string(),
      role: yup.string(),
      period: yup.string(),
      description: yup.string(),
    }),
  ),
  skills: yup.array().of(
    yup.object().shape({
      id: yup.string(),
      title: yup.string(),
    }),
  ),
})

const EditProfile = () => {
  const { id: profileId } = useParams()
  const profilesList = useAppSelector(state => state.profiles.list)
  const currentProfile = profilesList.find(it => it.id === profileId)
  const dispatch = useAppDispatch()

  const handleSubmit = (values: Profile, formik: FormikHelpers<Profile>) => {
    dispatch(updateProfile(values))

    notification.success({
      message: `Profile ${values.firstName} ${values.lastName} successfully updated`,
    })

    formik.setSubmitting(false)
  }

  return (
    <CommonLayout>
      <Title>Edit Profile: </Title>

      <Formik<Profile>
        initialValues={currentProfile!}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ dirty, isSubmitting, errors, isValid }) => (
          <Form>
            <Title type="danger" level={3}>
              {!isValid && Object.values(errors)}
            </Title>

            <Input name="firstName" placeholder="First Name" />
            <Input name="lastName" placeholder="Last Name" />
            <Input name="email" placeholder="Email" />
            <Input name="tagline" placeholder="Tagline" />

            <div style={{ margin: '30px 0' }}>
              <Title level={3}>Skills:</Title>

              <AddRowButton
                name="skills"
                style={{ marginBottom: 15 }}
                createNewRow={() => ({
                  id: nanoid(),
                  title: '',
                })}
              >
                Add
              </AddRowButton>
              <Table
                name="skills"
                rowKey={row => row.id}
                size="small"
                pagination={false}
                columns={[
                  {
                    title: 'Skill',
                    key: 'skill',
                    render: (text, record, i) => (
                      <Input name={`skills.${i}.title`} />
                    ),
                  },
                ]}
              />
            </div>

            <SubmitButton
              type="primary"
              style={{ marginTop: 10 }}
              disabled={!dirty || isSubmitting || !isValid}
            >
              Update Profile
            </SubmitButton>
          </Form>
        )}
      </Formik>
    </CommonLayout>
  )
}

export default EditProfile
