import { Table } from 'antd'
import Title from 'antd/lib/typography/Title'
import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { PROFILES_ROUTES } from '../../../constants/routes'
import CommonLayout from '../../../layouts/CommonLayout'
import { useAppSelector } from '../../../store'
import { Profile } from '../../../store/slices/profiles/profilesSlice'

const { CREATE_PROFILE, VIEW_PROFILE } = PROFILES_ROUTES

const columns = [
  {
    title: 'First name',
    dataIndex: 'firstName',
    key: 'firstName',
  },
  {
    title: 'First name',
    dataIndex: 'lastName',
    key: 'lastName',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
  {
    title: 'Tagline',
    dataIndex: 'tagline',
    key: 'tagline',
  },
]

const Profiles = () => {
  const profilesList = useAppSelector(state => state.profiles.list)
  const navigate = useNavigate()

  const handleRowClick = (profile: Profile) => {
    navigate(VIEW_PROFILE.buildPath(profile.id))
  }

  return (
    <CommonLayout>
      <Title>Profiles</Title>

      <Table
        dataSource={profilesList}
        columns={columns}
        pagination={false}
        onRow={(record, rowIndex) => {
          return { onClick: event => handleRowClick(record) }
        }}
      />

      <div style={{ marginTop: '30px' }}>
        <Link to={CREATE_PROFILE} style={{ fontSize: '24px' }}>
          Add new Profile
        </Link>
      </div>
    </CommonLayout>
  )
}

export default Profiles
