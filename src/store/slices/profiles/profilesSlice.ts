import { createSlice, PayloadAction } from '@reduxjs/toolkit'

type WorkExperienceProfile = {
  id: string
  company: string
  role: string
  period: string
  description: string
}

export type Profile = {
  id: string
  firstName: string
  lastName: string
  email: string
  tagline: string
  experiences?: WorkExperienceProfile[]
  skills: {
    id: string
    title: string
  }[]
}

type ProfilesSliceInitial = {
  list: Profile[]
}

const initialState: ProfilesSliceInitial = {
  list: [],
}

const profilesSlice = createSlice({
  name: 'profiles',
  initialState,
  reducers: {
    createNewProfile: (state, action: PayloadAction<Profile>) => {
      state.list.push(action.payload)
    },
    updateProfile: (state, action: PayloadAction<Profile>) => {
      const updatedProfile = state.list.findIndex(
        it => it.id === action.payload.id,
      )

      state.list[updatedProfile] = action.payload
    },
  },
})

export const { createNewProfile, updateProfile } = profilesSlice.actions

export default profilesSlice.reducer
